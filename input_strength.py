# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script sets the strength property for graphs with "self" implementation and zhao's implementation
# argument 1: "self" or "zhao"
# argument 2: threshold value

from graph_tool.all import *
import os
import sys

try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided (<Keyword> <Graph Type> <Threshold>)")
	
try:
	graphType = sys.argv[2]
except IndexError:
	sys.exit("No graph type provided (zhao/self)")
	
try:
	thresholdWeight = float(sys.argv[3])
except:
	sys.exit("No threshold provided (<Keyword> <Graph Type> <Threshold>)")
	
myPath = os.path.dirname(os.path.realpath(__file__))
theGraph = load_graph(myPath + "/graph/" + keyword + "_clean_"  + graphType + ".gml")

# create new edge property
e_strength = theGraph.new_edge_property("string")
for e in theGraph.edges():
	if theGraph.ep.weight[e] > thresholdWeight:
		e_strength[e] = "strong"
	else:
		e_strength[e] = "weak"

# store new property for .gml file		
theGraph.edge_properties["Strength"] = e_strength

gmlFileName = keyword + "_strength_" + graphType + ".gml"
theGraph.save(gmlFileName)

os.rename(myPath + '/' + gmlFileName, myPath + "/graph/" + gmlFileName)	


		