# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this file looks through all files in path, get rids of redundant "\n"
# creates a new file in /processed_data/chomped_data/

import os

minimalDataIndex = 11
path = "raw_data/"
dirs = os.listdir(path)
ordered_files = sorted(dirs, reverse=True)

# iterate through all files in directory, by alphabetical order
for file in ordered_files:
	dataFileName = str(file)
	myPath = os.path.dirname(os.path.realpath(__file__))
	finalFileName = myPath + "/processed_data/chomped_data/" + dataFileName[:-4] + '_chomped.txt'
	dataFileName = "raw_data/" + dataFileName
	dataFile = open(dataFileName, 'r', encoding='utf-8')
	
	finalFile = open(finalFileName, 'w', encoding='utf-8')
	iter = 1
	# iterate each line of a file
	for line in dataFile:
		if iter > 3:
			lineParts = line.split("\t")
			if (lineParts[0].isdigit()) and (int(lineParts[0]) != 0):
				try:
					testVariable = lineParts[minimalDataIndex]
					finalFile.write(line)
				except:
					theIndex = int(lineParts[0])
					newline = line
					while True:
						nextline = dataFile.readline()
						newline = newline.rstrip('\n')
						newline = newline + ' ' + nextline
						newLineParts = newline.split("\t")

						try:
							retweetID = newLineParts[minimalDataIndex]
							finalFile.write(newline)
							break
						except:
							continue
							

		else:
			finalFile.write(line)

		iter = iter + 1
		
	dataFile.close()
	
finalFile.close()
