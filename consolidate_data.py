# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script consolidates multiple data .txt files of the same hashtag/keyword and removes duplicated tweets

import os
import sys

listTweetIDs = set()

try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided")

filename = "Consolidated_Data_For_" + keyword + ".txt"
myPath = os.path.dirname(os.path.realpath(__file__))
finalFile = open(myPath + '/processed_data/' + filename, 'w', encoding='utf-8')

path = "processed_data/chomped_data/"
dirs = os.listdir(path)
ordered_files = sorted(dirs, reverse=True)

fileIter = 1
tweetIter = 1
for file in ordered_files:
	dataFileName = str(file)
	fileNameParts = dataFileName.split("_")
	if fileNameParts[2] == keyword:
		dataFileName = myPath + '/processed_data/chomped_data/' + dataFileName
		dataFile = open(dataFileName, 'r', encoding='utf-8')
		iter = 1
		for line in dataFile:
			if iter > 3:
				lineParts = line.split("\t")
				if (lineParts[0].isdigit()) and (int(lineParts[0]) != 0):
					if lineParts[1] not in listTweetIDs:
						listTweetIDs.add(lineParts[1])

						tweetStr = str(tweetIter)
						for i in range(1, len(lineParts)):
							tweetStr = tweetStr + '\t' + lineParts[i]

						finalFile.write(tweetStr)

						tweetIter = tweetIter + 1
				else:
					finalFile.write(line)
			iter = iter + 1
		dataFile.close()
	else:
		print("skipping file: {}".format(file))
	fileIter = fileIter + 1
finalFile.close()
