# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script converts zhao graphs into demeo graphs by removing all weight information

from graph_tool.all import *
import os
import sys


try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided (<keyword>")

myPath = os.path.dirname(os.path.realpath(__file__))

theGraph = load_graph(myPath + "/graph/" + keyword + "_clean_zhao" + ".gml")
saveFileName = keyword + "_clean_demeo" + ".gml"


for e in theGraph.edges():
	#remove previous values of weight
	theGraph.ep.weight[e] = 1

theGraph.save(saveFileName)
os.rename(myPath + '/' + saveFileName, myPath + "/graph/" + saveFileName)
