# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script checks the number of local bridges and how many of them are weak ties

from graph_tool.all import *
import os
import sys
from bst import BinarySearchTree

# keyword for data
try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided")
	
try:
	graphType = sys.argv[2]
except IndexError:
	sys.exit("No graph type provided (demeo/self)")

sys.setrecursionlimit(10000)	
myPath = os.path.dirname(os.path.realpath(__file__))
theGraph = load_graph(myPath + "/graph/" + keyword + "_strength_"  + graphType + ".gml")

class MyNode:
	
	def __init__(self, neighbourList, id):
		self.id = id
		self.neighbourList = neighbourList
		
	def addNeighbour(self, v1):
		self.neighbourList.append(v1)

# create node objects with neighbours
vertexBST = BinarySearchTree()
nodeClassList = []
gmlID = 0
for v in theGraph.vertices():

	neighbourList = []
	node = MyNode(neighbourList, gmlID)

	for neighbor in v.out_neighbours():
		node.addNeighbour(neighbor)
	
	v_userID = theGraph.vp.userID[v]
	
	vertexBST.put(int(v_userID), node)
	nodeClassList.append(node)
	gmlID = gmlID + 1

print("Done with nodes")
progressFile = "local_bridge_prog.txt"
logFile = open(progressFile, 'a')
logFile.write("Results for {} {}\n".format(keyword, graphType))
logFile.close()

# create new edge property
e_bridge = theGraph.new_edge_property("bool")

numEdges = theGraph.num_edges()
localBridgeCount = 0
weakCount = 0
weakBridge = 0
edgeCount = 0

# iterate all edges to see if local bridge is weak tie
for e in theGraph.edges():
	notLocalBridgeFlag = False
	edgeCount = edgeCount + 1
	
	
	v1 = e.source()
	v2 = e.target()
	v1Node = vertexBST.get(int(theGraph.vp.userID[v1]))
	v2Node = vertexBST.get(int(theGraph.vp.userID[v2]))
	
	# find local bridge
	for n1 in v1Node.neighbourList:
		for n2 in v2Node.neighbourList:
			if n1 == n2:
				notLocalBridgeFlag = True
				break
		if notLocalBridgeFlag is True:
			break
	if notLocalBridgeFlag is False:
		localBridgeCount = localBridgeCount + 1
		if theGraph.ep.Strength[e] == "weak":
			weakBridge = weakBridge + 1
		e_bridge[e] = True
	else:
		e_bridge[e] = False
	
	if edgeCount % 1000 == 0:
		logFile = open(progressFile, 'a')
		logFile.write("{}% done\n".format(edgeCount * 100 / numEdges))
		logFile.close()

logFile = open(progressFile, 'a')
try:
	logFile.write("{} weak local bridges out of {} local bridges: {:.2f}%\n".format(weakBridge, localBridgeCount, weakBridge*100/localBridgeCount))
except:
	logFile.write("{} weak local bridges out of {} local bridges: {}%\n".format(weakBridge, localBridgeCount, weakBridge*100/localBridgeCount))
logFile.close()
	
# store new property for .gml file		
theGraph.edge_properties["local_bridge"] = e_bridge

gmlFileName = keyword + "_bridge_" + graphType + ".gml"
logFile = open(progressFile, 'a')
logFile.write("saving gml file\n")
logFile.close()

theGraph.save(gmlFileName)

os.rename(myPath + '/' + gmlFileName, myPath + "/graph/" + gmlFileName)