# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this is the file that gathers collects raw data using multiple twitter developer authentication keys
# authentication keys are changed to dummy keys; previous keys in the repo have also been changed

from twython import Twython
from twython import TwythonError, TwythonRateLimitError, TwythonAuthError
import time
import datetime
import re
import sys
import os

APP_KEY = 'dummy_app_key_1'
APP_SECRET = 'dummy_app_secret_1'
OAUTH_TOKEN = 'dummy_oauth_token_1'
OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_1'

twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
mainIteration = 1
numOfTwitters = 11

def changeTwitter():
	global mainIteration
	global numOfTwitters
	global twitter
	if mainIteration % numOfTwitters == 1:
		APP_KEY = 'dummy_app_key_1'
		APP_SECRET = 'dummy_app_secret_1'
		OAUTH_TOKEN = 'dummy_oauth_token_1'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_1'
		print("Changed to Twitter 1")
	elif mainIteration % numOfTwitters == 2:
		APP_KEY = 'dummy_app_key_2'
		APP_SECRET = 'dummy_app_secret_2'
		OAUTH_TOKEN = 'dummy_oauth_token_2'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_2'
		print("Changed to Twitter 2")
	elif mainIteration % numOfTwitters == 3:
		APP_KEY = 'dummy_app_key_3'
		APP_SECRET = 'dummy_app_secret_3'
		OAUTH_TOKEN = 'dummy_oauth_token_3'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_3'
		print("Changed to Twitter 3")
	elif mainIteration % numOfTwitters == 4:
		APP_KEY = 'dummy_app_key_4'
		APP_SECRET = 'dummy_app_secret_4'
		OAUTH_TOKEN = 'dummy_oauth_token_4'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_4'
		print("Changed to Twitter 4")
	elif mainIteration % numOfTwitters == 5:
		APP_KEY = 'dummy_app_key_5'
		APP_SECRET = 'dummy_app_secret_5'
		OAUTH_TOKEN = 'dummy_oauth_token_5'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_5'
		print("Changed to Twitter 5")
	elif mainIteration % numOfTwitters == 6:
		APP_KEY = 'dummy_app_key_6'
		APP_SECRET = 'dummy_app_secret_6'
		OAUTH_TOKEN = 'dummy_oauth_token_6'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_6'
		print("Changed to Twitter 6")
	elif mainIteration % numOfTwitters == 7:
		APP_KEY = 'dummy_app_key_7'
		APP_SECRET = 'dummy_app_secret_7'
		OAUTH_TOKEN = 'dummy_oauth_token_7'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_7'
		print("Changed to Twitter 7")
	elif mainIteration % numOfTwitters == 8:
		APP_KEY = 'dummy_app_key_8'
		APP_SECRET = 'dummy_app_secret_8'
		OAUTH_TOKEN = 'dummy_oauth_token_8'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_8'
		print("Changed to Twitter 8")
	elif mainIteration % numOfTwitters == 9:
		APP_KEY = 'dummy_app_key_9'
		APP_SECRET = 'dummy_app_secret_9'
		OAUTH_TOKEN = 'dummy_oauth_token_9'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_9'
		print("Changed to Twitter 9")
	elif mainIteration % numOfTwitters == 10:
		APP_KEY = 'dummy_app_key_10'
		APP_SECRET = 'dummy_app_secret_10'
		OAUTH_TOKEN = 'dummy_oauth_token_10'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_10'
		print("Changed to Twitter 10")
	elif mainIteration % numOfTwitters == 0:
		APP_KEY = 'dummy_app_key_11'
		APP_SECRET = 'dummy_app_secret_11'
		OAUTH_TOKEN = 'dummy_oauth_token_11'
		OAUTH_TOKEN_SECRET = 'dummy_oauth_secret_11'
		print("Changed to Twitter 11")
	else:
		print("Error: Could not switch between Twitter keys")
	twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
	
def callForTwitterChange(apiCallCount):
	
	global mainIteration
	global numOfTwitters
	
	if mainIteration % numOfTwitters != 0:
			mainIteration = mainIteration + 1
			changeTwitter()
	else:
		log.write("it is now {} and prevID and lastID are {} and {}\n".format(str(datetime.datetime.today()), prevLastID, lastID))
		time.sleep(960)
		mainIteration = mainIteration + 1
		changeTwitter()
		
	apiCallCount = 0
	return apiCallCount
	
def checkFriendshipReply(output, sourceID, replyToID):

	global twitter
	
	friendship = twitter.show_friendship(source_id=sourceID, target_id=replyToID)
	#check if A who replys B is following B
	if friendship['relationship']['target']['followed_by'] is True:
		output.write("\t{}".format("501"))
	else:
		output.write("\t{}".format("500"))
	#check if B is following A	
	if friendship['relationship']['target']['following'] is True:
		output.write("\t{}".format("511"))
	else:
		output.write("\t{}".format("510"))

def checkFriendshipRetweet(output, sourceID, RTStatID):
	global twitter
	
	friendship = twitter.show_friendship(source_id=sourceID, target_id=RTStatID)
	#check if A who retweeted B is following B
	if friendship['relationship']['target']['followed_by'] is True:
		output.write("\t{}".format("601"))
	else:
		output.write("\t{}".format("600"))
	#check if B is following A	
	if friendship['relationship']['target']['following'] is True:
		output.write("\t{}".format("611"))
	else:
		output.write("\t{}".format("610"))

def checkFriendshipMention(output, sourceID, mentionID):
	friendship = twitter.show_friendship(source_id=sourceID, target_id=mentionID)
	#check if A who mentions B is following B
	if friendship['relationship']['target']['followed_by'] is True:
		output.write("\t{}".format("1"))
	else:
		output.write("\t{}".format("0"))
	#check if B is following A	
	if friendship['relationship']['target']['following'] is True:
		output.write("{}".format("1"))
	else:
		output.write("{}".format("0"))
		
	output.write("\t{}".format(mentionID))
	
def changeToLocal(dStr):
	hour = dStr[11] + dStr [12]
	intHour = int(hour)
	intHour = intHour + 8
	if intHour > 23:
		intHour = intHour - 24
		hour = '0' + str(intHour)
	elif (intHour == 9) or (intHour == 8):
		hour = '0' + str(intHour)
	else:
		hour = str(intHour)
	dList = list(dStr)	
	#print('hour is {}'.format(hour))
	dList[11] = hour[0]
	dList[12] = hour[1]
	dStr = ''.join(dList)
	return dStr



theNum = 1
try:
	keyword = sys.argv[1]
	keyword = '#' + str(keyword)
except IndexError:
	sys.exit("No keyword provided")

# forming of date time string to use for filename
tempDT = str(datetime.datetime.today())
tempDT = re.sub(r'[:.]', '-', tempDT)
tempDT = re.sub(r'[ ]', '_', tempDT)
fileKeyWord = keyword[1:]
dtNow = tempDT + '_' + fileKeyWord + '_hashtag' + '.txt'
myPath = os.path.dirname(os.path.realpath(__file__))
completeFilename = os.path.join(myPath+'/raw_data', dtNow)
output = open(completeFilename, 'w', encoding='utf-8')


output.write("Tweets of {}\n".format(keyword))
output.write ("{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n\n".format('number', 'tweet id', 'user_id', 'date', 'text', 'reply to ID', "RT-of ID", "replyFriendStatus1", "replyFriendStatus2", "retweetFriendStatus1", "retweetFriendStatus2", "no. of mentions", "mention status", "mention-of ID"))
completeLogName = os.path.join(myPath+'/raw_data', 'mineTweets_' + fileKeyWord + '_Log.txt')
log = open(completeLogName, 'w', encoding='utf-8')

lastID = -1
prevLastID = 0
apiCallCount = 0


while(lastID != prevLastID):

	prevLastID = lastID
	
	if apiCallCount > 170:
		apiCallCount = callForTwitterChange(apiCallCount)
		
	#rate limit 180 requests/ 100 tweets
	apiCallCount = apiCallCount + 1
	tweets = twitter.search(q=keyword, count=100, max_id=lastID)['statuses']
	
	for tweet in tweets:
		if tweet['in_reply_to_user_id']:
			replyToID = tweet['in_reply_to_user_id']
		else:
			replyToID = '0'

		try:
			RTStatID = tweet['retweeted_status']['user']['id']
		except KeyError:
			RTStatID = '0'
			
		createdTime = changeToLocal(tweet['created_at'])
		output.write("{}\t".format(theNum))
		output.write("{}\t".format(tweet['id']))
		output.write("{}\t".format(tweet['user']['id']))
		output.write("{}\t".format(createdTime))
		output.write("{}\t".format(tweet['text']))
		output.write("{}\t".format(replyToID))
		output.write("{}".format(RTStatID))

			
		############################## Look up on friendship ###############################
		
		if replyToID != '0':
			apiCallCount = apiCallCount + 1
			try:
				checkFriendshipReply(output, tweet['user']['id'], replyToID)
			except TwythonRateLimitError:
				apiCallCount = callForTwitterChange(apiCallCount)
				try:
					apiCallCount = apiCallCount + 1
					checkFriendshipReply(output, tweet['user']['id'], replyToID)
				except TwythonError as e:
					log.write("--{}--".format(e))
					output.write("\t{}\t{}".format("50-", "51-"))
					log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], replyToID))
					log.write("api call count is {}".format(apiCallCount))
				except:
					log.write("--Non Twython Error--")
					output.write("\t{}\t{}".format("50-", "51-"))
					log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], replyToID))
					log.write("api call count is {}".format(apiCallCount))
			
			except TwythonAuthError:
				log.write("--{}--".format(e))
				log.write("Occurred at tweet number {}".format(theNum))
			except TwythonError as e:
				log.write("--{}--".format(e))
				output.write("\t{}\t{}".format("50-", "51-"))
				log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], replyToID))
				log.write("api call count is {}".format(apiCallCount))
			
			except:
				log.write("--Non Twython Error--")
				output.write("\t{}\t{}".format("50-", "51-"))
				log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], replyToID))
				log.write("api call count is {}".format(apiCallCount))
		else:
			output.write("\t{}\t{}".format("-5", "-5"))
		
		#check if the tweet retweets to anyone
		if RTStatID != '0':
			apiCallCount = apiCallCount + 1			
			try:
				checkFriendshipRetweet(output, tweet['user']['id'], RTStatID)
				
			except TwythonRateLimitError:
				apiCallCount = callForTwitterChange(apiCallCount)
				try:
					apiCallCount = apiCallCount + 1
					checkFriendshipRetweet(output, tweet['user']['id'], RTStatID)
				except TwythonError as e:
					log.write("--{}--".format(e))
					output.write("\t{}\t{}".format("60-", "61-"))
					log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], RTStatID))
					log.write("api call count is {}".format(apiCallCount))
				except:
					log.write("--Non Twython Error--")						
					output.write("\t{}\t{}".format("60-", "61-"))
					log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], RTStatID))
					log.write("api call count is {}".format(apiCallCount))

			except TwythonAuthError:
				log.write("--{}--".format(e))
				log.write("Occurred at tweet number {}".format(theNum))
				
			except TwythonError as e:
				log.write("--{}--".format(e))
				output.write("\t{}\t{}".format("60-", "61-"))
				log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], RTStatID))
				log.write("api call count is {}".format(apiCallCount))
			except:
				log.write("--Non Twython Error--")						
				output.write("\t{}\t{}".format("60-", "61-"))
				log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], RTStatID))
				log.write("api call count is {}".format(apiCallCount))
			
		else:
			output.write("\t{}\t{}".format("-6", "-6"))
		################################# End of Friendship Check #########################

		################################# Check of Mentions ###############################
		mentionAutoCount = 0
		
		if bool(tweet["entities"]["user_mentions"]) is False:
			output.write("\t{}".format("0"))
		else:
			for mention in tweet["entities"]["user_mentions"]:
				mentionAutoCount = mentionAutoCount + 1
			output.write("\t{}".format(mentionAutoCount))
			for mention in tweet["entities"]["user_mentions"]:
				apiCallCount = apiCallCount + 1
				try:
					checkFriendshipMention(output, tweet['user']['id'], mention['id_str'])
				except TwythonRateLimitError:
					apiCallCount = callForTwitterChange(apiCallCount)
					try:
						apiCallCount = apiCallCount + 1
						checkFriendshipMention(output, tweet['user']['id'], mention['id_str'])

					except TwythonError as e:
						log.write("--{}--".format(e))
						log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], mention['id_str']))
						log.write("api call count is {}".format(apiCallCount))
						if mention['id_str'] is None:
							output.write("\t{}\t{}".format("-1", "-1"))
						else:
							output.write("\t{}\t{}".format("-1", mention['id_str']))
					except:
						log.write("--Non Twython Error--")
						log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], mention['id_str']))
						log.write("api call count is {}".format(apiCallCount))
						if mention['id_str'] is None:
							output.write("\t{}\t{}".format("-1", "-1"))
						else:
							output.write("\t{}\t{}".format("-1", mention['id_str']))
				except TwythonAuthError:
					log.write("--{}--".format(e))
					log.write("Occurred at tweet number {}".format(theNum))
				except TwythonError as e:
					log.write("--{}--".format(e))
					log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], mention['id_str']))
					log.write("api call count is {}".format(apiCallCount))
					if mention['id_str'] is None:
						output.write("\t{}\t{}".format("-1", "-1"))
					else:
						output.write("\t{}\t{}".format("-1", mention['id_str']))
				except:
					log.write("--Non Twython Error--")
					log.write("error occured at source {} and target {}\n".format(tweet['user']['id'], mention['id_str']))
					log.write("api call count is {}".format(apiCallCount))
					if mention['id_str'] is None:
						output.write("\t{}\t{}".format("-1", "-1"))
					else:
						output.write("\t{}\t{}".format("-1", mention['id_str']))
						
		######################### End of mentions check ##############################
		output.write("\n")
		theNum = theNum+1
		lastID = tweet['id']

		



output.write("File ended at {}".format(str(datetime.datetime.today())))
log.close()
output.close()		