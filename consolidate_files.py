# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script combines files of different hashtags and removes identical tweets
# 2 arguments for keywords of the file

import os
import sys

try:
	keyword1 = sys.argv[1]
except IndexError:
	sys.exit("No keyword(1) provided (<Keyword1> <Keyword2>)")
	
try:
	keyword2 = sys.argv[2]
except IndexError:
	sys.exit("No keyword(2) provided (<Keyword1> <Keyword2>)")

	
outputFilename = "Consolidated_Data_For_" + keyword1 + "_" + keyword2 + ".txt"
finalFile = open(outputFilename, 'w', encoding='utf-8')

myPath = os.path.dirname(os.path.realpath(__file__))

fileNameOfCData = myPath + "/processed_data/Consolidated_Data_For_" + keyword1 + ".txt"
file1 = open(fileNameOfCData, 'r', encoding='utf-8')
fileNameOfCData = myPath + "/processed_data/Consolidated_Data_For_" + keyword2 + ".txt"
file2 = open(fileNameOfCData, 'r', encoding='utf-8')


tweetCount = 1
sameTweetCount = 0
file1Flag = 0
file2Flag = 0
endFlag = 0
line_file1 = file1.readline()
line_file2 = file2.readline()


while endFlag == 0:
	
	# create current string on file 1 indexed by tweetCount
	if line_file1 != "":
		newLine_file1 = str(tweetCount)
		theList_file1 = line_file1.split('\t')
		lenList_file1 = len(theList_file1)
		for x in range(1, lenList_file1):
			newLine_file1 = newLine_file1 + '\t' + theList_file1[x]

	else:
		# finished with file 1
		file1Flag = 1
		
	# create current string on file 2 indexed by tweetCount	
	if line_file2 != "":
		newLine_file2 = str(tweetCount)
		theList_file2 = line_file2.split('\t')
		lenList_file2 = len(theList_file2)

		for x in range(1, lenList_file2):
			newLine_file2 = newLine_file2 + '\t' + theList_file2[x]

	else:
		# finished with file 2
		file2Flag = 1	
	
	# compare tweet ids so to write to output in descending order
	try:	
		if theList_file1[1] > theList_file2[1] and file1Flag == 0:
			finalFile.write(newLine_file1)
			line_file1 = file1.readline()
			tweetCount = tweetCount + 1
		elif theList_file1[1] < theList_file2[1] and file2Flag == 0:
			finalFile.write(newLine_file2)
			line_file2 = file2.readline()
			tweetCount = tweetCount + 1
		elif theList_file1[1] == theList_file2[1] and file2Flag == 0 and file1Flag == 0:
			finalFile.write(newLine_file1)
			print("tweets are the same at {}".format(theList_file1[1]))
			line_file2 = file2.readline()
			line_file1 = file1.readline()
			sameTweetCount = sameTweetCount + 1
			tweetCount = tweetCount + 1
	except:
		# index error: one file has completed its read
		pass
		
	# file1 is read completely, write the rest of file2	
	if file1Flag == 1 and file2Flag == 0:
		finalFile.write(newLine_file2)
		line_file2 = file2.readline()
		tweetCount = tweetCount + 1
	
	# file2 is read completely, write the rest of file1	
	if file2Flag == 1 and file1Flag == 0:
		finalFile.write(newLine_file1)
		line_file1 = file1.readline()
		tweetCount = tweetCount + 1
		
		
		
	if file1Flag == 1 and file2Flag == 1:
		endFlag = 1
		

file1.close()
file2.close()
finalFile.close()
print("Number of tweets is {}".format(tweetCount))
print("Removed {} similar tweets".format(sameTweetCount))
os.rename(myPath + '/' + outputFilename, myPath + "/processed_data/" + outputFilename)
	