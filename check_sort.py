# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script checks if the data file is sorted according to latest tweets at the top of the txt file
# by default it should only output "unsorted at 1" as the initial prevNumber is set at 0
# if the output is only "unsorted at 1" then the file is sorted

import os
import sys

try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided")
	
myPath = os.path.dirname(os.path.realpath(__file__))
inputFilename = myPath + "/processed_data/Consolidated_Data_For_" + keyword + ".txt"
output = open(inputFilename, 'r', encoding='utf-8')

prevNumber = 0
for line_file1 in output:
	theList = line_file1.split('\t')
	if prevNumber < int(theList[1]):
		print("unsorted at {}".format(theList[0]))
	elif prevNumber == int(theList[1]):
		print("same tweet at {}".format(theList[0]))
	prevNumber = int(theList[1])
output.close()
