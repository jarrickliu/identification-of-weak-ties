# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script takes in a .txt of nodes and edge information and makes a .gml file using Zhao's implementation
# the script uses 2 classes and a Binary Search Tree for better efficiency as graph_tool's iterator is slow

from graph_tool.all import *
import os
import sys
import re
import time
from bst import BinarySearchTree

try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided")

# class to represent vertex
# each node holds a list of vertices that are its neighbours
class MyNode:
	
	def __init__(self, neighbourList, id):
		self.id = id
		self.neighbourList = neighbourList
		
	def addNeighbour(self, nodeID):
		self.neighbourList.append(nodeID)

# class to represent edges 
# a Tie holds the node id that will be in the gml file		
class Tie:	
	def __init__(self, sourceGmlID, targetGmlID):
		self.sourceGmlID = sourceGmlID
		self.targetGmlID = targetGmlID

sys.setrecursionlimit(10000)		
myPath = os.path.dirname(os.path.realpath(__file__))
fileNameOfGraphInfo = myPath + "/graph/" + keyword + "_pre_gml_zhao" + ".txt"
inputTextFile = open(fileNameOfGraphInfo, 'r', encoding='utf-8')
progressFile = "zhao_progress.txt"
nodeCount = 0
edgeCount = 0

# key: twitter userID; value: gml id
vertexBST = BinarySearchTree()

theGraph = Graph(directed=False)

# creating node and edge properties
v_userID = theGraph.new_vertex_property("string")
e_weight = theGraph.new_edge_property("double")
e_ID = theGraph.new_edge_property("string")

# take in the total number for nodes
line = inputTextFile.readline()
if re.search("Total Nodes", line, re.IGNORECASE):
	totalNodesList = line.split(' ')
	nodeCount = int(totalNodesList[2].rstrip('\n'))

# create nodes in graph
# graph-tool creates ids for nodes starting from 0
nodeClassList = []
gmlID = 0	
while nodeCount != 0:
	line = inputTextFile.readline()
	v = theGraph.add_vertex()
	v_userID[v] = line.rstrip('\n')
	
	vertexBST.put(int(v_userID[v]), gmlID)
	
	neighbourList = []
	node = MyNode(neighbourList, gmlID)
	nodeClassList.append(node)
	
	nodeCount = nodeCount - 1
	gmlID = gmlID + 1

logFile = open(progressFile, 'a')
logFile.write("Done with nodes")
logFile.close()


# take in the total number for edges
line = inputTextFile.readline()	
if re.search("Total Edges", line, re.IGNORECASE):
	totalEdgesList = line.split(' ')
	edgeCount = int(totalEdgesList[2].rstrip('\n'))

# create edges in graph
tieClassList = []
while edgeCount != 0:
	# readline takes in twitter ID
	line = inputTextFile.readline()
	edgeList = line.split('\t')
	v1 = None
	v2 = None
	weight = int(edgeList[2].rstrip('\n'))
	
	if weight > 0:
		# retrieve the gml index of node using twitter ID
		v1GmlID = vertexBST.get(int(edgeList[0]))
		v2GmlID = vertexBST.get(int(edgeList[1]))
		
		# retrieve graph_tool vertex objects using gml index
		v1 = theGraph.vertex(v1GmlID)
		v2 = theGraph.vertex(v2GmlID)
		
		if v1 is None:
			print("Error: v1 {} not found".format(int(edgeList[0])))
		if v2 is None:	
			print("Error: v2 {} not found".format(int(edgeList[1])))
		
		# create actual graph on .gml file
		e = theGraph.add_edge(v1, v2)
		e_weight[e] = weight
		e_ID[e] = edgeList[0] + edgeList[1]
		
		# append Tie object for calculation of Zhao's weight
		tie = Tie(v1GmlID, v2GmlID)
		tieClassList.append(tie)
		
		# adding neighbours to the nodes' respective lists
		v1Node = nodeClassList[v1GmlID]
		v2Node = nodeClassList[v2GmlID]
		v1Node.addNeighbour(v2GmlID)
		v2Node.addNeighbour(v1GmlID)
		
		
	edgeCount = edgeCount - 1
	
logFile = open(progressFile, 'a')
logFile.write("Done with edges")
logFile.close()



# iterate through all edges and calculate weight according to Zhao's method
# i.e. weight = mutual friends / (V1's degree - V2) + (V2's degree - V1) - mutual friends
delEdgeList = []
edgeCount = 0
startTime = 0
total = theGraph.num_edges()
for tie in tieClassList:
	commonFriendCount = 0  # numerator
	denominator = 0
	edgeCount = edgeCount + 1
	
	gmlID1 = tie.sourceGmlID
	gmlID2 = tie.targetGmlID
	
	# retrieve graph tool vertex objects
	v1 = theGraph.vertex(gmlID1)
	v2 = theGraph.vertex(gmlID2)
	
	# retrieve MyNode objects
	node1 = nodeClassList[gmlID1]
	node2 = nodeClassList[gmlID2]
	
	e = theGraph.edge(v1, v2)
	
	# calculating Zhao's weight
	if len(node1.neighbourList) > 1 and len(node2.neighbourList) > 1:
		for n_v1 in node1.neighbourList:
			for n_v2 in node2.neighbourList:
				if n_v1 == n_v2:
					commonFriendCount = commonFriendCount + 1
		
		denominator = len(node1.neighbourList) - 1 + len(node2.neighbourList) - 1 - commonFriendCount
		if denominator != 0:
			e_weight[e] = float(commonFriendCount) / denominator
		else:
			# edges with denominator = 0 are to be deleted
			delEdgeList.append(e)
			
	elif len(node1.neighbourList) == 1 and len(node2.neighbourList) == 1:
		delEdgeList.append(e)
		
	else:
		e_weight[e] = 0
	
	if edgeCount % 1000 == 0:
		endTime = time.time()
		logFile = open(progressFile, 'a')
		logFile.write("calculating edge: {}\n".format(edgeCount))
		timeTaken = endTime - startTime
		print("calculating edge: {}".format(edgeCount))
		print("{:.2f}% done; need {} seconds more".format(edgeCount*100/total, timeTaken * (total-edgeCount)/1000))
		logFile.close()
		startTime = time.time()
		
logFile = open(progressFile, 'a')
logFile.write("Done with giving weight to edges")
logFile.close()

# delete the edges with 0 as denominator	
print("Deleting Edges with 0 as denominator")
while delEdgeList:
	e = delEdgeList.pop()
	theGraph.remove_edge(e)
	
logFile = open(progressFile, 'a')
logFile.write("Done with deleting edges")
logFile.close()
	
theGraph.vertex_properties["userID"] = v_userID
theGraph.edge_properties["weight"] = e_weight
theGraph.edge_properties["edgeID"] = e_ID

logFile = open(progressFile, 'a')
logFile.write("saving gml")
logFile.close()

print("Saving gml file")
gmlFileName = keyword + "_zhao" + ".gml"
theGraph.save(gmlFileName)

os.rename(myPath + '/' + gmlFileName, myPath + "/graph/" + gmlFileName)
os.remove(myPath + '/' + progressFile)
inputTextFile.close()
		
	