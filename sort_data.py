# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script sorts the data file with newer tweets on top of the txt file

import time
import datetime
import re
import os
import sys


try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided")

myPath = os.path.dirname(os.path.realpath(__file__))
inputFilename = myPath + "/processed_data/Consolidated_Data_For_" + keyword + ".txt"

outputFilename = "Consolidated_Data_For_" + keyword + ".txt"
input = open(inputFilename, 'r')
finalFile = open(outputFilename, 'w')
bigList = []

# get all tweet IDs; larger tweet ids indicate newer tweets
for line in input:
	theList = line.split('\t')
	bigList.append(theList[1])
	
input.close()

bigList.sort(reverse = True)
theLength = len(bigList)
tweetCount = 1
iter = 0
done = 0

# use bigList as a reference of tweet ids in order
# and write tweets to output file from latest to oldest
while done==0:

	input = open(inputFilename, 'r')
	for line in input:
		theList = line.split('\t')
		if int(theList[1]) == int(bigList[iter]):
			newLine = str(tweetCount)
			lenList = len(theList)
			for x in range(1, lenList):
				newLine = newLine + '\t' + theList[x]
			finalFile.write(newLine)
			tweetCount = tweetCount + 1
			iter = iter + 1 
			if iter == theLength:
				break
	input.close()

	if iter == theLength:
		done = 1

os.rename(inputFilename, myPath + "/processed_data/archive/Consolidated_Data_For_" + keyword + "_beforeSort.txt")
os.rename(myPath + '/' + outputFilename, myPath + "/processed_data/" + outputFilename)
finalFile.close()