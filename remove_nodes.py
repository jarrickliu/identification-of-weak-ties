# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script removes all nodes with 0 degrees from the .gml files
# this is so to prepare the gml file for cascade.py i.e. information diffusion simulations

from graph_tool.all import *
import os
import sys

try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided (<keyword> <graph type>)")
	
try:
	graphType = sys.argv[2]
except IndexError:
	sys.exit("No graph type provided")
	
myPath = os.path.dirname(os.path.realpath(__file__))

theGraph = load_graph(myPath + "/graph/" + keyword + "_" + graphType + ".gml")
saveFileName = keyword + "_clean_" + graphType + ".gml"

delList = []
gmlIDCounter = 0

for vertex in theGraph.vertices():
	
	if vertex.out_degree() == 0:
		delList.append(gmlIDCounter)
	gmlIDCounter = gmlIDCounter + 1

zeroDegreeCount = len(delList)		
delList.sort()

while delList:
	delVertex = theGraph.vertex(delList.pop())
	theGraph.remove_vertex(delVertex)
		

theGraph.save(saveFileName)
os.rename(myPath + '/' + saveFileName, myPath + "/graph/" + saveFileName)	
print("The total number of vertices with 0 degree is {}".format(zeroDegreeCount))

	
	