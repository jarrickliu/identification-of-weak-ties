# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script creates a .txt file that makes it more efficient for graph-tool to plot graphs [Zhao's method]

from graph_tool.all import *
import os
import sys
from bst import BinarySearchTree

try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided")

sys.setrecursionlimit(10000)
myPath = os.path.dirname(os.path.realpath(__file__))
inputFilename = myPath + "/processed_data/Consolidated_Data_For_" + keyword + ".txt"
input = open(inputFilename, 'r', encoding='utf-8')
outputFilename = keyword + "_pre_gml_zhao.txt"
output = open(outputFilename, 'w', encoding='utf-8')
setOfFriendship = set() 
listOfIDs = set()

theBST = BinarySearchTree()

tweetCount = 0

class Friendship:
	
	def __init__(self, weight, id):
		self.id = id
		self.weight = weight
		
def createFriendship(interactionType, userNodeID, corresNodeID):
	if int(userNodeID) < int(corresNodeID):
		numEdgeID = userNodeID + corresNodeID
		edgeID = userNodeID + '\t' + corresNodeID
	elif int(userNodeID) > int(corresNodeID):
		numEdgeID = corresNodeID + userNodeID 
		edgeID = corresNodeID + '\t' + userNodeID
	else:
		return 0

	# create new edge 
	if numEdgeID not in setOfFriendship:
		setOfFriendship.add(numEdgeID)
		e = Friendship(0, edgeID)

		theBST.put(int(numEdgeID), e)
			
		if interactionType == 5:
			#if user follows corresNode [the e_code doesnt seem necessary]
			if theList[7] == "501":
				e.weight = e.weight + 1
			#if user is followed by corresNode	
			if theList[8] == "511":
				e.weight = e.weight + 1


		elif interactionType == 6:
			if theList[9] == "601":
				e.weight = e.weight + 1	
			if theList[10] == "611":
				e.weight = e.weight + 1


		elif interactionType >= 11:
			if theList[mentionsIter-1] == "11":
				e.weight = e.weight + 2
			elif theList[mentionsIter-1] == "10" or theList[mentionsIter-1] == "01":
				e.weight = e.weight + 1

		else:
			print("Error at new edge: interactionType is {}".format(interactionType))
				



			
for line in input:
	
	try:
		theList = line.split("\t")
		try:
			if (theList[0].isdigit()) and (int(theList[0]) != 0):

				tweetCount = tweetCount + 1
				
				if theList[2] not in listOfIDs:
					listOfIDs.add(theList[2])
				
				#find/insert node for user-reply
				if theList[5].isdigit() and int(theList[5]) != 0:
					if theList[5] not in listOfIDs:
						listOfIDs.add(theList[5])

					createFriendship(5, theList[2], theList[5])
					

				#find/insert node for user-retweet
				if theList[6].isdigit() and int(theList[6]) != 0:			
					if theList[6] not in listOfIDs:
						listOfIDs.add(theList[6])

					
					createFriendship(6, theList[2], theList[6])

				
				
				#find/insert node for mentions
				lengthOfList = len(theList)
				theList[lengthOfList-1] = theList[lengthOfList-1].rstrip('\n')
				mentionsIter = 11 # 11th element onwards of theList is mentions ID
				while mentionsIter+1 != lengthOfList:
					mentionsIter = mentionsIter + 2
					if theList[mentionsIter].isdigit() and int(theList[mentionsIter]) != 0 and (theList[mentionsIter] != theList[5] and theList[mentionsIter] != theList[6]):
						if theList[mentionsIter] not in listOfIDs:
							listOfIDs.add(theList[mentionsIter])


						createFriendship(mentionsIter, theList[2], theList[mentionsIter])
						
		except:
			if theList[0] != "":
				print("exception: list[0] is {}\n".format(theList[0]))
	except UnicodeEncodeError:
		output.write("failedhere\n")

output.write("Total Nodes: {}\n".format(len(listOfIDs)))

while len(listOfIDs) != 0:
	nodeID = listOfIDs.pop()
	output.write("{}\n".format(nodeID))

output.write("Total edges: {}\n".format(len(setOfFriendship)))

while len(setOfFriendship) != 0:
	tempEdgeID = setOfFriendship.pop()
	e = theBST.get(int(tempEdgeID))
	output.write("{}\t{}\n".format(e.id, e.weight))
	

os.rename(myPath + '/' + outputFilename, myPath + "/graph/" + outputFilename)	
input.close()
output.close()