# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script takes in a .txt of nodes and edge information and makes a .gml file using the proposed method

from graph_tool.all import *
import os
import sys
import re
import time
from bst import BinarySearchTree

try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided")
	
sys.setrecursionlimit(10000)
myPath = os.path.dirname(os.path.realpath(__file__))
fileNameOfGraphInfo = myPath + "/graph/" + keyword + "_pre_gml_self" + ".txt"
inputTextFile = open(fileNameOfGraphInfo, 'r', encoding='utf-8')
nodeCount = 0
edgeCount = 0

# key: twitter userID; value: gml id
vertexBST = BinarySearchTree()

theGraph = Graph(directed=False)

# creating node and edge properties
v_userID = theGraph.new_vertex_property("string")
e_weight = theGraph.new_edge_property("int64_t")
e_ID = theGraph.new_edge_property("string")

# take in the total number for nodes
line = inputTextFile.readline()
if re.search("Total Nodes", line, re.IGNORECASE):
	totalNodesList = line.split(' ')
	nodeCount = int(totalNodesList[2].rstrip('\n'))

# create nodes in graph
# graph-tool creates ids for nodes starting from 0
gmlIDCounter = 0	
while nodeCount != 0:
	line = inputTextFile.readline()
	v = theGraph.add_vertex()
	v_userID[v] = line.rstrip('\n')
	vertexBST.put(int(v_userID[v]), gmlIDCounter)	
	nodeCount = nodeCount - 1
	gmlIDCounter = gmlIDCounter + 1

# take in the total number for edges
line = inputTextFile.readline()	
if re.search("Total Edges", line, re.IGNORECASE):
	totalEdgesList = line.split(' ')
	edgeCount = int(totalEdgesList[2].rstrip('\n'))

	
startTime = 0
total = edgeCount
# create edges in graph	
while edgeCount != 0:
	line = inputTextFile.readline()
	edgeList = line.split('\t')
	v1 = None
	v2 = None
	
	# retrieve the gml index of node using BST as it is faster than graph-tool's default iterator
	v1 = theGraph.vertex(vertexBST.get(int(edgeList[0])))
	v2 = theGraph.vertex(vertexBST.get(int(edgeList[1])))
	if v1 is None:
		print("v1 {} not found".format(int(edgeList[0])))
	if v2 is None:	
		print("v2 {} not found".format(int(edgeList[1])))
		
	e = theGraph.add_edge(v1, v2)
	e_weight[e] = int(edgeList[2].rstrip('\n'))
	e_ID[e] = edgeList[0] + edgeList[1]
	edgeCount = edgeCount - 1

	if edgeCount % 1000 == 0:
		endTime = time.time()
		timeTaken = endTime - startTime
		print("calculating edge: {}".format(edgeCount))
		print("{:.2f}% done; need {} seconds more".format((total-edgeCount)*100/total, timeTaken * edgeCount/1000))
		startTime = time.time()
	
theGraph.vertex_properties["userID"] = v_userID
theGraph.edge_properties["weight"] = e_weight
theGraph.edge_properties["edgeID"] = e_ID

print("Saving gml file")
gmlFileName = keyword + "_self" + ".gml"
theGraph.save(gmlFileName)

	
	
os.rename(myPath + '/' + gmlFileName, myPath + "/graph/" + gmlFileName)	
inputTextFile.close()
		
	