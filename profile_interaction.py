# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script finds out the number of retweets, replies and mentions and their average

from graph_tool.all import *
import os
import sys

try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided")

myPath = os.path.dirname(os.path.realpath(__file__))
inputFilename = myPath + "/processed_data/Consolidated_Data_For_" + keyword + ".txt"
input = open(inputFilename, 'r', encoding='utf-8')

tweetCount = 0
retweetCount = 0
replyCount = 0
mentionCount = 0
totalMentions = 0
mentionsPerTweet = 0
freqHistogramForMentions = [0] * 20

for line in input:
	
	try:
		theList = line.split("\t")
		try:
			if (theList[0].isdigit()) and (int(theList[0]) != 0):
				
				tweetCount = tweetCount + 1
				mentionsPerTweet = 0
				
				
				# check if tweet is a reply
				if theList[5].isdigit() and int(theList[5]) != 0:
					replyCount = replyCount + 1
					

				# check if tweet is a retweet
				if theList[6].isdigit() and int(theList[6]) != 0:			
					retweetCount = retweetCount + 1

				
				
				# check if tweet is a mention (exclude reply and retweet)
				lengthOfList = len(theList)
				theList[lengthOfList-1] = theList[lengthOfList-1].rstrip('\n')
				mentionsIter = 11 # 11th element onwards of theList is mentions ID
				mentionFlag = False
				while mentionsIter+1 != lengthOfList:
					mentionsIter = mentionsIter + 2
					if theList[mentionsIter].isdigit() and int(theList[mentionsIter]) != 0 and (theList[mentionsIter] != theList[5] and theList[mentionsIter] != theList[6]):
						totalMentions = totalMentions + 1
						mentionFlag = True
						mentionsPerTweet = mentionsPerTweet + 1
						
				# indicates this tweet is a mention tweet
				if mentionFlag is True:
					mentionCount = mentionCount + 1
				
				# increase list value to get frequency of mentions
				freqHistogramForMentions[mentionsPerTweet] = freqHistogramForMentions[mentionsPerTweet] + 1
				
		except:
			if theList[0] != "":
				print("exception: list[0] is {}\n".format(theList[0]))
	except UnicodeEncodeError:
		print("UnicodeEncodeError")

print("Num Reply Tweets: {} Percentage of Replies Tweets: {}".format(replyCount, replyCount/tweetCount))
print("Num Retweet Tweets: {} Percentage of Retweet Tweets: {}".format(retweetCount, retweetCount/tweetCount))
print("Num Mention Tweets: {} Percentage of Mention Tweets: {}".format(mentionCount, mentionCount/tweetCount))
print("Average mention per mention tweet: {}".format(totalMentions/mentionCount))

# get histogram in terms of percentage

for i in range(1, 20):
	print("{0:.2f}% ".format(freqHistogramForMentions[i]*100/mentionCount), end="")
print("\n")

input.close()
