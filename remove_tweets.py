# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script uses keywords found in text clusters that by analytics software 'khcoder' to remove tweets that are from UK's general elections

import time
import datetime
import re
import os
import sys

try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided")

	
myPath = os.path.dirname(os.path.realpath(__file__))
filenameRead = myPath + "/processed_data/Consolidated_Data_For_" + keyword + ".txt"
filename = "Consolidated_Data_For_" + keyword + ".txt"
input = open(filenameRead, 'r')
finalFile = open(filename, 'w')
# this was done so that the if-else would remove those with "bbc" but not "bbcnewsasia"
inverseBBC = "bbcnewsasia"
# keywords to remove tweets
removalList = ['voteukip', 'voteukip2015', 'ukge2020', 'ukip', 'ukiplover456', 'rejoinworld', 'gh2015', 'bbc']

tweetCount = 0
removedCounter = 0
for line in input:
	theList = line.split('\t')
	removeCount = 0
	# if tweet has "bbcnewsasia", the tweet is not removed
	if re.search(inverseBBC, theList[4], re.IGNORECASE):
		tweetCount = tweetCount + 1
		newLine = str(tweetCount)
		lenList = len(theList)
		for x in range(1, lenList):
			newLine = newLine + '\t' + theList[x]
		finalFile.write(newLine)
	else:
		# look through the tweet for each keyword in the removal list
		# if the keyword is in the tweet, the tweet is not written to the output file
		for i in range(0, len(removalList)):
			if re.search(removalList[i], theList[4], re.IGNORECASE):
				removeCount = removeCount + 1
				removedCounter = removedCounter + 1
				break
		# if nothing has been removed from above, the tweet is written to the output file
		if removeCount == 0:
			tweetCount = tweetCount + 1
			newLine = str(tweetCount)
			lenList = len(theList)
			for x in range(1, lenList):
				newLine = newLine + '\t' + theList[x]
			finalFile.write(newLine)
print("Removed tweets: {}".format(removedCounter))

os.rename(filenameRead, myPath + "/processed_data/archive/Consolidated_Data_For_" + keyword + "_afterRemove.txt")
os.rename(myPath + '/' + filename, myPath + "/processed_data/" + filename)
input.close()
finalFile.close()
