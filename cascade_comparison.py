# Author: Liu Chengkai (https://bitbucket.org/jarrickliu/)

# this script is a modification of cascade.py to compare de meo and zhao's implementations
# this scripts simulates information diffusion through independent cascade model
# user is to provide filename and probability of infection and 
# the minimum of Graph A(weak ties, strong ties) and Graph B(weak ties, strong ties)
# 0.05% of vertices to are randomly selected as seed nodes

# this script currently only compares de meo and zhao's implementations

from graph_tool.all import *
import os
import sys
import time
from random import randint
import random
import numpy
from bst import BinarySearchTree

# keyword for data
try:
	keyword = sys.argv[1]
except IndexError:
	sys.exit("No keyword provided (<Keyword> <Graph Type> <Graph type> <Number of Tests Per Percentage> <minimum number of ties>)")

# type of implementation for weight/weightless
try:
	graphType1 = str(sys.argv[2])
except IndexError:
	sys.exit("No graph type provided (<Keyword> <Graph Type> <Graph type> <Number of Tests Per Percentage> <minimum number of ties>)")
	
try:
	graphType2 = str(sys.argv[3])
except IndexError:
	sys.exit("No graph type provided (<Keyword> <Graph Type> <Graph type> <Number of Tests Per Percentage> <minimum number of ties>)")
	
# number of tests per percentage
try:
	numTest = int(sys.argv[4])
except IndexError:
	sys.exit("No number of tests provided (<Keyword> <Graph Type> <Graph Type> <Number of Tests Per Percentage> <minimum number of ties>)")

# number of minimum ties
try:
	minimumCount = int(sys.argv[5])
except IndexError:
	sys.exit("No minimum ties set(<Keyword> <Graph Type> <Graph Type> <Number of Tests Per Percentage> <minimum number of ties>)")
	
	
# Tie object holds both source and target nodes of an edge
class Tie:
	
	def __init__(self, sourceNode, targetNode):
		self.sourceNode = sourceNode
		self.targetNode = targetNode
	
outputFilename = keyword + "_CoverageComparison_" + graphType1 + "_" + graphType2 +".txt"
progressFile = "cascade_progress.txt"
prog = open(progressFile, 'a', encoding='utf-8')
output = open(outputFilename, 'a', encoding='utf-8')
output.write("started at {}\n".format(time.asctime(time.localtime(time.time()))))
prog.write("started at {}\n".format(time.asctime(time.localtime(time.time()))))


myPath = os.path.dirname(os.path.realpath(__file__))
listGraphType = []
listGraphType.append(graphType1)
listGraphType.append(graphType2)

for graphType in listGraphType:
	theGraph = load_graph(myPath + "/graph/" + keyword + "_strength_"  + graphType + ".gml")
	if theGraph.num_vertices() == 0 or theGraph.num_edges() == 0:
		sys.exit("Invalid Graph")
	theGraphBackup = theGraph.copy()
	# list of all average coverage percentage
	allAverageCoverage = []

	weakTiesBST = BinarySearchTree()
	strongTiesBST = BinarySearchTree()
	weakEdgeList = []
	strongEdgeList = []

	# inserting weak and strong ties to BST
	# BST key: edgeID; value: Tie object

	for e in theGraph.edges():
		if theGraph.ep.Strength[e] == "weak":
			weakEdgeList.append(int(theGraph.ep.edgeID[e]))
			weakTie = Tie(e.source(), e.target())
			weakTiesBST.put(int(theGraph.ep.edgeID[e]), weakTie)
		elif theGraph.ep.Strength[e] == "strong":
			strongEdgeList.append(int(theGraph.ep.edgeID[e]))
			strongTie = Tie(e.source(), e.target())
			strongTiesBST.put(int(theGraph.ep.edgeID[e]), strongTie)
		else:
			prog.write("Error: neither weak nor strong")

	weakCount = len(weakEdgeList)
	strongCount = len(strongEdgeList)

		
	# removal of % of weak ties and then strong ties
	# starting at 5% at increasing at 'interval'
	i = 5
	interval = 5
	while i != 200:
		
		# --------------------- Getting current percentage ------------------------------
		coverageList = []
		percentage = 0.0
		
		if i < 100:
			percentage = float(i) / 100
			output.write("------------- Removing {} weak ties; ".format(int(percentage * minimumCount)))
		else:
			percentage = float(i-100) / 100
			output.write("------------- Removing {} strong ties; ".format(int(percentage * minimumCount)))
		if minimumCount != strongCount:
			output.write("({}% of weak ties) -----------\n".format(percentage*100))
		elif minimumCount != weakCount:
			output.write("({}% of strong ties) -----------\n".format(percentage*100))
		else:
			output.write("({}% of strong/weak ties) -----------\n".format(percentage*100))
		
		prog.write("Percentage: {}%\n".format(percentage*100))
		print("Now at percentage: {}%".format(percentage*100))
		
		# -------------------- Run test at the current percentage for numTest times ---------------
		currTest = 0
		while currTest != numTest:
			
			theGraph = theGraphBackup.copy()	
			e_probability = theGraph.new_edge_property("double")
			v_infected = theGraph.new_vertex_property("int")
			
			totalNumNodes = theGraph.num_vertices()
			deleteTiesSet = set()
			deleteSetTuple = ()
			newlyAffected = set()
			staticAffected = []
			tempNewlyAffected = []
			tempCounter = 0
			
			
			#----------------------- removal of ties ------------------------------
			# adding source/target values of edges to be deleted to deleteTiesSet
			
			# i < 100 for the iterations where weak ties are removed
			if i < 100:
				while len(deleteTiesSet) != int(percentage * minimumCount):
					for key in weakEdgeList:
						if random.uniform(0.0, 1.0) < percentage:
							deleteTie = weakTiesBST.get(key)
							delEdge = theGraph.edge(deleteTie.sourceNode, deleteTie.targetNode)
							if int(theGraph.vp.userID[delEdge.source()]) < int(theGraph.vp.userID[delEdge.target()]):
								deleteSetTuple = (delEdge.source(), delEdge.target())
							elif int(theGraph.vp.userID[delEdge.source()]) > int(theGraph.vp.userID[delEdge.target()]):
								deleteSetTuple = (delEdge.target(), delEdge.source())
							else:
								prog.write("Equal user id {} and {} for edge {}\n".format(int(theGraph.vp.userID[delEdge.source()]), int(theGraph.vp.userID[delEdge.target()]), theGraph.ep.edgeID[delEdge]))
								continue
							deleteTiesSet.add(deleteSetTuple)

						if len(deleteTiesSet) == int(percentage * minimumCount):
							break

			else:
				while len(deleteTiesSet) != int(percentage * minimumCount):
					for key in strongEdgeList:
						if random.uniform(0.0, 1.0) < percentage:
							deleteTie = strongTiesBST.get(key)
							delEdge = theGraph.edge(deleteTie.sourceNode, deleteTie.targetNode)
							if int(theGraph.vp.userID[delEdge.source()]) < int(theGraph.vp.userID[delEdge.target()]):
								deleteSetTuple = (delEdge.source(), delEdge.target())
							elif int(theGraph.vp.userID[delEdge.source()]) > int(theGraph.vp.userID[delEdge.target()]):
								deleteSetTuple = (delEdge.target(), delEdge.source())
							else:
								prog.write("Equal user id {} and {} for edge {}\n".format(int(theGraph.vp.userID[delEdge.source()]), int(theGraph.vp.userID[delEdge.target()]), theGraph.ep.edgeID[delEdge]))
								continue
							deleteTiesSet.add(deleteSetTuple)
							
						if len(deleteTiesSet) == int(percentage * minimumCount):
							break
						
			prog.close()			
			prog = open(progressFile, 'a', encoding='utf-8')
			
			# actual removal of ties from deleteTiesSet
			actualDelCount = 0
			while deleteTiesSet:
				actualDelCount = actualDelCount + 1
				deleteSetTuple = deleteTiesSet.pop()
				thisEdge = theGraph.edge(deleteSetTuple[0], deleteSetTuple[1])
				theGraph.remove_edge(thisEdge)
			prog.write("Deleted {} edges\n".format(actualDelCount))
				
			# random selection of infected vertices (0.05% of total vertices)
			numInitialAffected = int(0.0005 * totalNumNodes)
			if numInitialAffected == 0:
				numInitialAffected = 1
			while len(newlyAffected) != numInitialAffected:
				randomSeed = randint(0, totalNumNodes-1)		
				thisNode = theGraph.vertex(randomSeed)
				newlyAffected.add(thisNode)
			prog.write("Selected {} infected seeds\n".format(numInitialAffected))
			
			# set infection status for seed nodes
			for infected in newlyAffected:
				v_infected[infected] = 1
				tempCounter = tempCounter + 1
				
			# set edge probability of infection
			for edge in theGraph.edges():
				edgeInfectProb = random.uniform(0.0, 1.0)
				e_probability[edge] = edgeInfectProb 
				
			# set all vertex to unaffected status
			for vertex in theGraph.vertices():
				v_infected[vertex] = 0 

			# iterate all newly infected nodes
			# perform infection using BFS
			while newlyAffected:
				infected = newlyAffected.pop()
				
				# get all edges connected to the infected node
				allEdges = infected.all_edges()
				
				# iterate all target nodes connected to infected node to test for infection
				# once an edge is 'used' for infection, it cannot be used again
				for connectingEdge in allEdges:
					if e_probability[connectingEdge] > 0:
						if connectingEdge.source() == infected:
							targetNode = connectingEdge.target()
						elif connectingEdge.target() == infected:
							print("Target is the infected\n")
							targetNode = connectingEdge.source()
						else:
							print("Error: Neither nodes connected to the edge is infected\n")
						randomFloat = random.uniform(0.0, 1.0)
						
						if randomFloat < e_probability[connectingEdge] and v_infected[targetNode] != 1:
							tempNewlyAffected.append(targetNode)
							tempCounter = tempCounter + 1
							v_infected[targetNode] = 1
						#this is so that each edge will only be processed once	
						e_probability[connectingEdge] = -1.0
						
				staticAffected.append(infected)
				while tempNewlyAffected:
					tempInfect = tempNewlyAffected.pop()
					newlyAffected.add(tempInfect)
			
			currTest = currTest + 1
			output.write("Test {}: ".format(currTest))
			output.write("Coverage is {:.2f}%\n".format((len(staticAffected))/totalNumNodes*100))
			coverageList.append(float(len(staticAffected))/totalNumNodes*100)
			
			
		i = i + interval
		# skip at 100%
		if i == 100:
			i = i + interval
				
		# calculate the average of coverageList
		coverageList.sort()
		numberOfCover = 0
		totalPerc = 0.0
		stdev = numpy.std(coverageList)
		mean = numpy.mean(coverageList)
		
		for coverage in coverageList:
			if abs(coverage - mean) < stdev:
				totalPerc = totalPerc + coverage
				numberOfCover = numberOfCover + 1
		if numberOfCover == 0:
			sys.exit("There are too little tests resulting in an inability to calculate the mean coverage or the graph is too small")
		output.write("Average is {:.2f}%\n".format(totalPerc/numberOfCover))
		output.write("\n")
		allAverageCoverage.append(totalPerc/numberOfCover)

	output.write("------------- All average coverage -------------\n ")	
	for average in allAverageCoverage:
		output.write("{}\n".format(average))
	
output.close()
prog.close()	
os.rename(myPath + '/' + outputFilename, myPath + "/results/" + outputFilename)
os.remove(myPath + '/' + progressFile)
		

	